// Mini plugins para JQuery

// Função existe, usada nos seletores
jQuery.fn.exists = function() {
    return this.length > 0;
}

// Corresponde à ucfirst
function ucfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function setpoint(x, y) {
    var divst = "position: absolute;";
    divst += "top: " + y + "px;";
    divst += "left: " + x + "px;";
    divst += "color: yellow;";

    var len = $('#components').children('div.x').length;
    if (len <= 9) {
        $('#components').append("<div class='x' style='" + divst + "'>x</div>");
    }
}

// Calcula distância entre pontos
function getDistance(btn, evt) {

    var x = parseInt(btn.css('left')) + (parseInt(btn.css('width')) / 2); // identifica o X do botão
    var y = parseInt(btn.css('top')) + (parseInt(btn.css('height')) / 2); // identifica o Y do botão
    var x2 = evt.clientX + evt.view.scrollX - 30; // captura o X do Mouse
    var y2 = evt.clientY + evt.view.scrollY - 45; // captura o Y do Mouse
//    setpoint(x,y);
    return Math.sqrt(Math.pow(parseInt(x) - parseInt(x2), 2) + Math.pow(parseInt(y) - parseInt(y2), 2));
}


$(document).ready(function() {

    // Declara Variáveis
    var inactiveRightConfirmOpacity = 0.35;

    // Declara funcões

    /**
     * Torna o botão direito opaco
     * @param JQuery btn
     */
    var setOpac = function(btn) {
        btn.addClass('opac');
        btn.removeClass('notopac');
    };

    /**
     * Torna o botão direito transparente
     * @param JQuery btn
     */
    var setNotopac = function(btn) {
        btn.addClass('notopac');
        btn.removeClass('opac');
    };

    /**
     * Returna true se o botao direito está checado
     * @param JQuery btn
     * @return boolean Se o botao está checado
     */
    var isRightChecked = function(btn) {
        return btn.children('img.ok_right').exists();
    };

    /**
     * Returna true se o botao direito está ativado
     * @param JQuery btn
     * @return boolean Se o botao está ativado
     */
    var isRightActived = function(btn) {
        return btn.hasClass('btnRAcitved');
    };

    /**
     * Returna true se o botao direito está confirmado, ou seja, se sua 
     * <i>modal</i> foi aberta.
     * @param JQuery btn Botão a ser verificado
     * @return boolean Se o botao está ativado
     */
    var isConfirmed = function(btn) {
        return btn.children('img.ok_right').exists();
    };

    /**
     * Retorna true se as identificações dos elementos são iguais
     * @param JQuery el1
     * @param JQuery el2
     * @return boolean Se os elemetos são iguais
     */
    var equals = function(el1, el2) {
        return el1.attr('id') === el2.attr('id');
    };

    console.log("O sistema está sendo inicializado");

    // Botão pra abrir e fechar menu em celulares
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });

    var mapHilightConf =
            {
                fill: true,
                fillColor: 'ff0000',
                fillOpacity: 0.2,
                stroke: true,
                strokeColor: 'ff0000',
                strokeOpacity: 0.5,
                strokeWidth: 1,
                fade: true,
                alwaysOn: false,
                neverOn: false,
                groupBy: "grupo",
                wrapClass: true,
                shadow: true,
                shadowX: 10,
                shadowY: 10,
                shadowRadius: 20,
                shadowColor: '000000',
                shadowOpacity: 0.7,
                shadowPosition: 'outside',
                shadowFrom: false
            };
    // Ativa o plugin ma placa mãe
    $('.map').maphilight(mapHilightConf);

    // Posiciona o componente
    $('#processador_btn').css({
        left: 445,
        top: 230
    });

    // Posiciona o componente
    $('#bateria_btn').css({
        left: 49,
        top: 369
    });

    // Posiciona o componente
    $('#pci_btn').css({
        left: 75,
        top: 152
    });

    // Posiciona o componente
    $('#soquetes_btn').css({
        left: 399,
        top: 415
    });

    // Posiciona o componente
    $('#ponteSul_btn').css({
        left: 85,
        top: 420
    });

    // Posiciona o componente
    $('#conectorFonte_btn').css({
        left: 490,
        top: 110
    });

    // Posiciona o componente
    $('#usb_btn').css({
        left: 512,
        top: 50
    });

    // Posiciona o componente
    $('#portasIde_btn').css({
        left: 279,
        top: 489
    });

    // Torna os botões da direita transparentes
    $(".btnRight").addClass('notopac');

    // Adiciona evento de hover nas áreas
    $('area').mouseover(function() {
        // Destaca o Confirmador à direita
        setOpac($('#' + $(this).attr("grupo") + "_right"));
        // Quando sair o mouse
    }).mouseout(function() {
        // Pega o Confirmador à direita
        var right = $('#' + $(this).attr("grupo") + "_right");
        // Se o mesmo Não está checado e não está ativado
        if ((!isRightChecked(right)) && (!isRightActived(right))) {
            // Volta a ser transparente
            setNotopac(right);
        }
        // Ativa as àreas com confirmador ativado
        $('.btnRight.btnRAcitved').each(function() {
            var id = $(this).attr("id").split("_")[0];
            $("[grupo=" + id + "]").first().trigger("mouseenter");
        });
        // Quando a área for clicada
    }).click(function() {
        // Pega o confirmador à direita
        var right = $('#' + $(this).attr("grupo") + "_right");
        // Se o mesmo Não está confirmado (Não tem nenhum botão dentro)
        if (!isConfirmed(right)) {
            // Confirma (Adiciona botão)
            // right.prepend('<div class="btn btn-xs btn-primary pull-right">OK</div>');
            right.prepend('<img class="pull-right ok_right" src="img/ok-2.png">');
            right.toggleClass("checked");
            right.toggleClass("unchecked");
            // Garante a opacidade
            right.animate({opacity: 1}, 105)
        }
        // Modifica o conteúdo da Modal com o Hidden Text do componente
        $('#myModalBody').html($("#hddTxt_" + $(this).attr("grupo")).html());
        $('#myModalLabel').html($("#" + $(this).attr("grupo") + "_right > strong").html());
        // Mostra a modal
        $('#myModal').modal({});
    }).qtip({
        style: {
            tip: 'bottomLeft'
        },
        position: {
            adjust: {
                y: -30
            }
        },
        events: {
            show: function(event, api) {
                api.set({
                    'content.text': api.elements.target.attr('title')
                });
            },
            hide: function(event, api) {
                console.log(event);
            }
        }
    });



    // Eventos acerca dos botões do lado direito
    $(".btnRight").mouseover(function(evt) {
        // Se saiu de um elemento que está dentro dele mesmo
        var from = evt.relatedTarget ? evt.relatedTarget : evt.fromElement;
        if (from !== undefined && from.parentNode === this) {
            // Não acontece nada
            return;
        }
        // Quando passar o mouse por cima de um botão, ativar a sua área
        var id = $(this).attr("id").split("_")[0];
        $("[grupo=" + id + "]").first().trigger("mouseenter");
    }).mouseout(function(evt) {
        // Se saiu para um elemento que está dentro dele mesmo
        var e = evt.toElement || evt.relatedTarget;
        if (e !== null && e.parentNode === this || e === this) {
            // Não acontece nada
            return;
        }
        // Se o botão não está ativado
        if (!isRightActived($(this))) {
            // Desativa as áreas
            var id = $(this).attr("id").split("_")[0];
            $("[grupo]").trigger("mouseleave");
            // Se houver algum botao direito ativo, reativa as áreas
            var btnRAcitved = $(".btnRAcitved");
            if (btnRAcitved.exists()) {
                // Ativa a área correspondente e ativa
                var idActive = btnRAcitved.first().attr("id").split("_")[0];
                $("[grupo=" + idActive + "]").first().trigger("mouseover");
            }
        }
    }).click(function(evt) {
        var clickedBtnRight = $(this);
        // Pega o grupo correspondente
        var id = clickedBtnRight.attr("id").split("_")[0];
        // Se o botão estava ativado, 
        if (clickedBtnRight.hasClass('btnRAcitved')) {
            // Abre a modal
            $("[grupo=" + id + "]").first().trigger("click");
            // Se o botão estava desativado
        } else {
            // Desativa todos os botões
            $(".btnRight").removeClass('btnRAcitved');

            // Se algum não tiver checado e não for o botão clicado
            $(".btnRight").each(function(el, idx) {
                if ((!isRightChecked($(this))) && (!equals($(this), clickedBtnRight))) {
                    // Volta a ser transparente
                    setNotopac($(this));
                }
            });
            // Desativa todas as áreas
            $('[grupo]').trigger('mouseout');

            // Ativa a área correspondente
            var id = clickedBtnRight.attr("id").split("_")[0];
            $("[grupo=" + id + "]").first().trigger("mouseenter");

        }
        // Se estava ativado desativa, senão, desativa
        $(this).toggleClass('btnRAcitved');
    });

// Evento de Clique na página
}).click(function(evt) {
//    console.log(evt.clientX-$("#img1").offset().left);
//    console.log(evt.clientY-$("#img1").offset().top);
//    console.log("----------------------");
});